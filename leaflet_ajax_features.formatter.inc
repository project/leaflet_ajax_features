<?php

/**
 * @file
 * Provides a field formatter for displaying map features using features.
 */

/**
 * Implements hook_field_formatter_info().
 */
function leaflet_ajax_features_field_formatter_info() {
  $formatter = array();
  $leaflet_formatter = leaflet_field_formatter_info();

  if (!empty($leaflet_formatter)) {
    $formatter['leaflet_ajax_features'] = array(
      'label' => t('Leaflet Ajax Features'),
      'field types' => array('geofield'),
      'settings' => $leaflet_formatter['geofield_leaflet']['settings'],
    );
  }

  return $formatter;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function leaflet_ajax_features_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $entity_type = $instance['entity_type'];

  $element = array();

  if ($display['type'] == 'leaflet_ajax_features') {
    $options = array('' => t('-- Select --'));
    foreach (leaflet_map_get_info() as $key => $map) {
      $options[$key] = $map['label'];
    }

    $element['leaflet_map'] = array(
      '#title' => t('Leaflet Map'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $settings['leaflet_map'],
      '#required' => TRUE,
    );

    $element['height'] = array(
      '#title' => t('Map Height'),
      '#type' => 'textfield',
      '#default_value' => $settings['height'],
      '#field_suffix' => t('px'),
      '#element_validate' => array('element_validate_integer_positive'),
    );

    foreach ($form['#fields'] as $fieldname) {
      $field_options[$fieldname] = $fieldname;
    }
    $fieldpath = 'fields[' . $field['field_name'] . '][settings_edit_form][settings]';
    $element['popup'] = leaflet_form_elements('popup', $settings, array('path' => $fieldpath));
    $element['zoom'] = leaflet_form_elements('zoom', $settings);
    $element['icon'] = leaflet_form_elements('icon', $settings, array('path' => $fieldpath, 'fields' => $field_options));
    $element['vector_display'] = leaflet_form_elements('vector_display', $settings, array('path' => $fieldpath));
    // Show the list of available tokens.
    $element['tokens'] = leaflet_form_elements('tokens', $settings, array('weight' => 999, 'entity_type' => $entity_type));
  }

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function leaflet_ajax_features_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = '';

  if ($display['type'] == 'leaflet_ajax_features') {
    $summary = t('Leaflet map: @map', array('@map' => $settings['leaflet_map']));
  }

  return $summary;
}

/**
 * Implements hook_field_formatter_view().
 */
function leaflet_ajax_features_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $settings = $display['settings'];

  if (count($items) == 0) {
    return '';
  }
  else {
    switch ($display['type']) {
      case 'leaflet_ajax_features':
        $element[0] = array(
          '#theme' => 'leaflet_ajax_features_map',
          '#settings' => $settings,
          '#map_type' => 'field',
          '#url_settings' => array(
            'entity_type' => $entity_type,
            'entity_id' => leaflet_ajax_features_get_entity_id($entity_type, $entity),
            'field_name' => $field['field_name'],
          ),
        );
        break;
    }

    return $element;
  }
}
