<?php

/**
 * @file
 * Leaflet map Views integration with AJAX feature load.
 */

/**
 * Implements hook_views_plugins().
 */
function leaflet_ajax_features_views_views_plugins() {
  $plugins = array(
    'module' => 'leaflet_ajax_features_views',
    'style' => array(
      'leaflet_ajax_features_views' => array(
        'title' => t('Leaflet Map (AJAX Feature Load)'),
        'help' => t('Displays a View as a Leaflet map with features loaded via AJAX.'),
        'handler' => 'leaflet_ajax_features_views_plugin_style',
        'path' => drupal_get_path('module', 'leaflet_ajax_features_views'),
        'theme' => 'leaflet-map',
        'uses fields' => TRUE,
        'uses row plugin' => FALSE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'type' => 'normal',
        'even empty' => TRUE,
      ),
    ),
  );

  return $plugins;
}